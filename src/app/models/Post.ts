export class Post {
  public id: number;
  public title: string;
  public content: string;
  public love_score: number;
  public created_at: Date;

  constructor( id: number, title: string, content: string) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.love_score = 0;
    this.created_at = new Date();
  }
}
