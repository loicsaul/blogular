import { Post } from './../models/Post';
import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss']
})
export class PostItemComponent implements OnInit, OnDestroy {
  @Input() post: Post;
  postsSubscription: Subscription;
  posts: Post[];

  constructor(
    private postService: PostsService,
  ) { }

  ngOnInit(): void {
    this.postsSubscription = this.postService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );

    this.postService.emitPostsSubject();
  }

  onLoveIt(point: number): void {
    this.postService.manageLoveScore(this.post.id, point);
  }

  onDeletePost(id: number): void {
    this.postService.removePost(id);
  }

  ngOnDestroy(): void {
    this.postsSubscription.unsubscribe();
  }
}
