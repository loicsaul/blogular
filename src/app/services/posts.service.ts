import { Post } from './../models/Post';
import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  postsSubject = new Subject<Post[]>();
  private loremIpsum: string;
  private posts: Post[];

  constructor() {
    this.loremIpsum     = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
    this.loremIpsum    += 'Nunc accumsan quam lorem, ac sollicitudin justo rutrum non';
    this.loremIpsum    += 'In laoreet egestas.';

    this.posts = [
      {
        id: 0,
        title: 'Les 10 plus beaux clous de l\'univers',
        content: this.loremIpsum,
        love_score: 0,
        created_at: new Date()
      },
      {
        id: 1,
        title: 'OH MON DIEU : MAIS QUE FAIT-IL ????',
        content: this.loremIpsum,
        love_score: 0,
        created_at: new Date()
      },
      {
        id: 2,
        title: 'Vous devinerez jamais ce qu\'il est capable de faire avec son nez.',
        content: this.loremIpsum,
        love_score: 0,
        created_at: new Date()
      },
      {
        id: 3,
        title: 'Il a assisté à la mort d\'un oppossum... Sa réaction est loufoque.',
        content: this.loremIpsum,
        love_score: 0,
        created_at: new Date()
      },
      {
        id: 4,
        title: 'Il mange des sequoïa avec ses sourcils !',
        content: this.loremIpsum,
        love_score: 0,
        created_at: new Date()
      }
    ];

    this.emitPostsSubject();
  }

  emitPostsSubject(): void {
    this.postsSubject.next(this.posts.slice());
  }

  setPostId(): number {
    return this.posts.length + 1;
  }

  addPost(newPost: Post) {
    this.posts.push(newPost);
    this.emitPostsSubject();
  }

  removePost(id: number): void {
    this.posts = this.posts.filter(post => post.id !== id);
    this.emitPostsSubject();
  }

  manageLoveScore(id: number, point: number) {
    this.posts[id].love_score += point;
    this.emitPostsSubject();
  }
}
